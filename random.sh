#!/bin/bash

numbers=(1 2 3 4 5 6 7 8 9 10)
counter=0

while [ $counter -le 9 ]
do
        rand=$(( ( RANDOM % 10 )  + 1 ))
        for number in ${numbers[@]}
        do
                i=0
                if [[ $number -eq $rand ]]
                then
                        echo $rand
                        counter=$(( $counter + 1 ))
                        delete=$number
                        numbers=( "${numbers[@]/$delete}" )
                fi
                i=$(( $i + 1 ))
        done
done
